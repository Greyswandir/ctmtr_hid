#ifndef TMTR_DEV_H
#define TMTR_DEV_H
#include<QString>
//#include<QObject>
//extern int outlog(int level, char *fmt, ...);
//extern int out_pkt(int level, char *buf, int len);

// Class ITmtrDev
class ITmtrDev {

public:
    virtual ~ITmtrDev() {}
    virtual QString getDevName (void) = 0;  // получить название устройства
    virtual int getDevVersion (void) = 0;   // получить версию устройства (-1 Unknown)
    virtual int getDevCode (void) = 0;      // получить код устройства (-1 Unknown)
    virtual int getDevPower (void) = 0;     // получить уровень заряда батареи
    virtual int getDevState (void) = 0;     // получить состояние устойства 0-ok
    virtual int getDTn (int n) = 0;         // получить интервал дискретизации по номеру (мк—)
    virtual int open (void) = 0;            // открыть устройтсво
    virtual void close (void) = 0;          // закрыть устройтсво
    virtual int isOpen (void) = 0;          // рроверка IsOpen
    virtual int setDT (int n) = 0;          // установка частоты цифровки
    virtual int getDT (void) = 0;           // получить интервал дискретизации (мк—)
    virtual int readU (double *u) = 0;      // получить напряжение на датчике (мк+)

    virtual double u2r (double u) = 0;      // получить сопротивление(kOm) по значению u(B)
};
#endif // TMTR_DEV_H
