QT       += core gui
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets
TARGET = task01
TEMPLATE = app

SOURCES += main.cpp\
        mainwindow.cpp \
    ctmtr_hid.cpp \
    hid.c

HEADERS  += mainwindow.h \
    ctmtr_hid.h \
    tmtr_dev.h \
    hidapi.h

FORMS    += mainwindow.ui

win32:LIBS += -lhid -lsetupapi
unix:LIBS += -lusb
