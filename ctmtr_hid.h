#ifndef CTMTR_HID_H
#define CTMTR_HID_H

#include "tmtr_dev.h"
#if defined(__linux) || defined(__linux__)
#include <libusb.h>
#else
//#include "libusb/libusb.h"
#include "hidapi.h"
#endif

class CTmtr_hid : public ITmtrDev
{
public:
    CTmtr_hid();
    virtual ~CTmtr_hid();

    virtual QString getDevName (void);  // получить название устройства
    virtual int getDevVersion (void);   // получить версию устройства (-1 Unknown)
    virtual int getDevCode (void);      // получить код устройства (-1 Unknown)
    virtual int getDevPower (void);     // получить уровень заряда батареи
    virtual int getDevState (void);     // получить состояние устойства 0-ok
    virtual int getDTn (int n);         // получить интервал дискретизации по номеру (мк—)
    virtual int open (void);            // открыть устройтсво
    virtual void close (void);          // закрыть устройтсво
    virtual int isOpen (void);          // рроверка IsOpen
    virtual int setDT (int n);          // установка частоты цифровки
    virtual int getDT (void);           // получить интервал дискретизации (мк—)
    virtual int readU (double *u);      // получить напряжение на датчике (мк+)

    virtual double u2r (double u);      // получить сопротивление(kOm) по значению u(B)

protected:
    int sendCmd (char *bsnd, int slen);

    double m_R0;
    double m_E0;
    double m_E1;
    double m_E;

    int m_f_open;
    hid_device *m_device;
    int m_dt;
    int m_dev_code;
    int m_deviceVersionNumber;

    QString m_deviceBaseName;
};

#endif // CTMTR_HID_H
