#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "tmtr_dev.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_openButton_clicked();

    void on_closeButton_clicked();

    void on_readButton_clicked();

private:
    Ui::MainWindow *ui;
    ITmtrDev *dev;
};

#endif // MAINWINDOW_H
