// tmtr.cpp : implementation of the CTmtr class

//#include "stdafx.h"

#include <process.h>
#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include <math.h>

#include <setupapi.h>
#include <initguid.h> 

#include "flog.h"
#include "app_cfg.h"

#include "fbtt.h"
#include "ff2.h"

#include "ws.h"

#include "tmtr_dev.h"
#include "tmtr.h"

#include "tmtr_dev_hid.h"
#include "tmtr_dev_ftdi.h"
#include "tmtr_dev_bt3.h"
#include "tmtr_dev_wifi4.h"
#include "tmtr_dev_ws.h"
#include "tmtr_dev_emu.h"

#ifdef USE_BLE
#include "tmtr_dev_ble.h"
#else
#include "tmtr_dev_exe.h"
#endif

#include "srv.h"

// class CTmtr

CTmtr::CTmtr(int len_buf,              // ����� ��������� � ��������� ������  
             int priority_enable,      // ���� ��������� ���������� 
             void (*call_back)(void),  // ������� ���������� ����� ���������� n_call ���������
             int n_call)               // ����� ��������� ��� ������ ������� call_back()  
{
   m_len_buf = len_buf;                // ����� ��������� � ��������� ������  
   m_call_back = call_back;            // ������� ���������� ����� ���������� n_call ���������
   if(n_call<1)
   {
      n_call = 1;
   }
   m_n_call=n_call;                    // ����� ��������� ��� ������ ������� call_back()   
   m_priority_enable=priority_enable;  // ���� ��������� ���������� 

   m_device_type = 0;            // ��� ���������� 
   m_dev_found = 0;              // ���� ���������� �������  

   m_n_dt = 0;                   // ����� ������� 
   m_dt = 10000.0;               // �������� ������������� (mkC) 

   m_hMutex = 0;                 // ������������� ������� � ������ ���������
   m_adc_next = 0;               // ���� "����� �������" 
   m_adc_in = 0;                 // ������ ������ ���������
   m_adc = new SBUF[m_len_buf];  // ����� ���������

   m_adc_last_u = 0.0;         // ���������(�������) ���������(V)    
   m_adc_last = 0.0;           // ���������(�������) ���������  
   m_adc_last_r = 0.0;         // ��������� (������� �� �������) ���������  
   m_adc_last_p = 0.0;         // ���� ��������� ���������   
   m_adc_last_val = 0.0;       // ���������(�������) ��������� �������

   m_thread_end = 0;           // ���� ��������� ���������
   m_thread_loop = 0;          // ���� ����� ��������� 

   m_enabeld = 0;              // CallBack enabled

   m_check_enable = 0;         // ���� ������ ���������� 

   m_tmtr_dev = 0;             // ����� ���������

   m_filter_mode = 1;           // ��� ������� (1,2,3) 
   m_f1 = 0;                    // ������� HPF
   m_f2 = 0;                    // ������� LPF
   m_fbtt1 = new fbtt();        // ������ HPF 
   m_fbtt2 = new fbtt();        // ������ LPF  
   m_fbtt_ind = new ff2();      // ������ �������

   m_trim = 0.0;            // ��������
   m_booster = 1.0;         // ����������� ����������� 
   m_gain = 1.0;            // ��������� ���������� ��������
   m_tone = 3.0;            // ��������� ����� ����
   m_corr = 1.0;            // ����������� �������� ��������

   m_ta_counter=0.0;      // ������� TA   
   m_ta_counter0=0.0;     // ������� TA  
   m_nm_counter=0.0;      // ������� NM  

   m_fo = 0;                // ���� ��� ������ ������  

   m_thread_srv_udp_loop = 0;  // ���� ����������� ������ UDP �������
   m_thread_srv_udp_end = 0;   // ���� ���������� ������ UDP �������
   m_thread_srv_ws_loop = 0;   // ���� ����������� ������ WS �������
   m_thread_srv_ws_end = 0;    // ���� ���������� ������ WS �������

   /*
   m_use_ble = 0;
   HMODULE hm = GetModuleHandle(_T("BluetoothApis.dll"));
   if (hm)
   {
       m_use_ble = 1;
   }
   */
   m_use_ble = TestBLE();
}


CTmtr::~CTmtr()
{

   if(m_fbtt1)
   {
      delete m_fbtt1;
   }

   if(m_fbtt2)
   {
      delete m_fbtt2;
   }

   if(m_fbtt_ind)
   {
      delete m_fbtt_ind;
   }

   if(m_tmtr_dev)
   {
      delete m_tmtr_dev;
      m_tmtr_dev=0;
   }

   if(m_adc)
   {
      delete [] m_adc;
      m_adc=0;
   }
}

// ����������� BLE
int CTmtr::IsUseBle(void)
{
   return m_use_ble;
}

// ������/���������� ����������
int CTmtr::RestartDev(int device_type)
{
   int i;

   //m_pTmtr->SetEnableCallBack(0);

   if(m_tmtr_dev)
   {
       DevThreadStop();      // ���������� �������� ���������
   }

   m_device_type = device_type;

   switch(device_type)
   {
   case DEV_ANY:
      for(i=DEV_FTDI;i<DEV_EMULER;i++)
      {
         m_tmtr_dev=0;
         switch(i)
         {
         case DEV_FTDI:
            m_tmtr_dev=new CTmtrDev_ftdi(0);
            break;
         case DEV_HID:
            m_tmtr_dev=new CTmtrDev_hid(0);
            break;
/*
         case DEV_BT3:
            m_tmtr_dev=new CTmtrDev_bt3(0);
            break;

         case DEV_WIFI4:
            m_tmtr_dev=new CTmtrDev_wifi4(0);
            break;
#ifdef USE_BLE
         case DEV_BLE:
	    m_tmtr_dev = new CTmtrDev_ble(0);
	    break;
#endif
*/
         }

         if(m_tmtr_dev)
	 {
            m_tmtr_dev->Open();
            if(m_tmtr_dev->IsOpen())
            {
               outlog(1,"Found device type=%d",i);
               m_device_type = i;
               m_dev_found = 1;
               //m_tmtr_dev->Close();
               break;
            }
            else
            {
               outlog(1, "Device type=%d not opened", i);
               delete m_tmtr_dev;
               m_tmtr_dev=0;
            }
         }
      }
      break;

   case DEV_FTDI:
      m_tmtr_dev=new CTmtrDev_ftdi(0);
      break;
   case DEV_HID:
      m_tmtr_dev=new CTmtrDev_hid(0);
      break;
   case DEV_BT3:
      m_tmtr_dev=new CTmtrDev_bt3(0);
      break;
   case DEV_WIFI4:
      m_tmtr_dev=new CTmtrDev_wifi4(0);
      break;

   case DEV_BLE:
       if (m_use_ble)
       {
#ifdef USE_BLE
           m_tmtr_dev = new CTmtrDev_ble(0);
#else
           m_tmtr_dev = new CTmtrDev_exe(0);
#endif
       }
      break;

   case DEV_WS_CLI:
      m_tmtr_dev=new CTmtrDev_ws(0);
      break;

   case DEV_EMULER:
      m_tmtr_dev=new CTmtrDev_emu(0);
      break;

   default:
      outlog(1,"Device type=%d failed",device_type);
   }

   if (m_tmtr_dev)
   {
      m_tmtr_dev->Open();
      if (m_tmtr_dev->IsOpen())
      {
         outlog(1, "Found device type=%d", device_type);
         m_dev_found = 1;
         //m_tmtr_dev->Close();
      }
      else
      {
         outlog(1, "Device type=%d not opened", device_type);
      }
   }

   m_dt = 10000.0;
   if(m_tmtr_dev)
   {
      m_dt=m_tmtr_dev->GetDTn(m_n_dt);
   }

   // ����� ��������� �������������
   SetIndexDT(papp_cfg->get_int_value(L"Mode\\index_dt"));
   // ��������� ������� ����������
   SetFilter(papp_cfg->get_int_value(L"Mode\\filter_mode"), papp_cfg->get_float_value(L"Mode\\inertia"), 0);

   //m_pTmtr->SetEnableCallBack(1);

   DevThreadStart();  // ������ �������� ���������

   return 0;
}


 // �������� ��� ����������
int CTmtr::GetDevType(void)
{
   return m_device_type;
}

// �������� ������ ���������� (-1 Unknown)
int CTmtr::GetDevVersion(void)
{
   if(m_tmtr_dev)
   {
      return m_tmtr_dev->GetDevVersion();
   }
   return 0;
}

// �������� �������� ����������
char* CTmtr::GetDevName(void)
{
   char *pc="No device";

   if(m_tmtr_dev)
   {
     pc=m_tmtr_dev->GetDevName();
   }

   return pc;
}

// �������� ��� ���������� 
int CTmtr::GetDevCode(void)
{
   if(m_tmtr_dev)
   {
      return m_tmtr_dev->GetDevCode();
   }
   return -1;
}

// �������� ������� ������ ������� 
int CTmtr::GetDevPower(void)
{
   if(m_tmtr_dev)
   {
      return m_tmtr_dev->GetDevPower();
   }
   return -1;
}

 // �������� ��������� ��������� 0-ok
int CTmtr::GetDevState(void)
{
   if(m_tmtr_dev)
   {
      return m_tmtr_dev->GetDevState();
   }
   return -1;
}



// �������� �������� ������������� ��� ������� ����� 'n' (���)
double CTmtr::GetDTn(int n)
{
   if(m_tmtr_dev)
   {
      return (double)m_tmtr_dev->GetDTn(n);
   }
   return 0;
}


// �������� �������� ������������� � ���
double CTmtr::GetDT(void)
{
   return m_dt;
}

// ����� ��������� ������������� (index: [0-2])
int CTmtr::SetIndexDT(int index)
{
   if(index >=0 && index<=2)
   {
      m_n_dt = index;
      return 1;
   }
   return 0;
}


// ��������� ������ ����������
void CTmtr::SetFilter(int mode, double f0, double f01)
{
   m_filter_mode = mode;
   m_f1=f01;
   m_f2=f0;

   // �������� ��������
   filter_ini();
}

// ��������� ������������� ������� ������
void CTmtr::SetIndMinMax(double min, double max)
{
   if(m_fbtt_ind)
   {
      m_fbtt_ind->set_mm(min, max);
   }
}


// ���������� �������� ������������ ���������
void CTmtr::SetBooster(double val)
{
   m_booster=val;
}

// ���������� �������� ���������� "trim"
void CTmtr::SetTrim(double val)
{
   m_trim=val;
}

/*
// ���������� ��� ������ (0->TA, 1->log10(R/.005)
void CTmtr::SetTypeTA(int type)
{
   m_type_ta=type;
}
*/

// ���������� �������� ������������ ��������
void CTmtr::SetGain(double val)
{
   m_gain=val;
}

// ���������� �������� ����� ���� 
void CTmtr::SetTone(double val)
{
   m_tone=val;
}


// ���������� �������� ������������ ��������� ��������
void CTmtr::SetCorr(double val)
{
   m_corr=val;
}


 // ���������� �������� �������� TA
void CTmtr::SetTaCounter(double val)
{
   m_ta_counter0=m_ta_counter;
   m_ta_counter=val;
}
   
// ���������� �������� �������� NM
void CTmtr::SetNmCounter(double val)
{
    //m_ta_counter=val;
   m_nm_counter=val;
}

// ���������� ������� ����������
void CTmtr::MeterCheck(int enable)
{
   m_check_enable=enable;
}


double CTmtr::GetTone(void)
{
   return m_tone;
}

double CTmtr::GetGain(void)
{
   // High TA sensitivity correction.
   // ���� TA>3,5, �� Sc(compensated)=S*X*(1+TA-3,5)=S*X*(TA-2,5), ��� S v�������
   double s = m_gain*m_booster;

   if(m_tone>3.5 && m_corr>0.0)
   {
      s*=m_corr*(m_tone-2.5);
   }

   return s;
}

// ������� ����������
int CTmtr::DevOpen(void)
{
   if(m_tmtr_dev)
   {
      return m_tmtr_dev->Open();
   }
   return 0;
}

// ������� ����������
void CTmtr::DevClose(void)
{
   if(m_tmtr_dev)
   {
      m_tmtr_dev->Close();
   }
}

// �������� ��������� ������ ����������
int CTmtr::DevIsFound(void)
{
   return m_dev_found;
}


// �������� IsOpen 
int CTmtr::DevIsOpen(void)
{
   if(m_tmtr_dev)
   {
      return m_tmtr_dev->IsOpen();
   }
   return 0;
}


// ��������� ������� ��������
int CTmtr::DevSetDt(int n)
{
   int rc;

   if(!m_tmtr_dev)
   {
      return 0;
   }

   if(!DevIsOpen())
   {
      return 0;
   }

   rc=m_tmtr_dev->SetDT(n);
   if(rc)
   {
      m_dt=m_tmtr_dev->GetDT();
      m_n_dt=n;
   }

   if(m_dt==0)
   {
      return 0;
   }

   // �������� ��������
   filter_ini();

   if(m_f1)
   {
      m_fbtt1->clr();
   }

   if(m_f2)
   {
      m_fbtt2->clr();
   }
   
   return rc;
}

// ??? 
void CTmtr::SetEnableCallBack(int on)
{
   m_enabeld=on;
}

int CTmtr::GetEnableCallBack(void)
{
   return m_enabeld;
}


// �������� ��������� ���������� �������(V) 
double CTmtr::GetU(void)
{
   if(!DevIsOpen())
   {
      m_adc_last_u=0.0;
   }
   return m_adc_last_u;
}

// �������� ��������� ���������� �������(kOm) 
double CTmtr::GetR(void)
{
   if(!DevIsOpen())
   {
      m_adc_last_u=0.0;
   }

   return u2r(m_adc_last_u);
}


// �������� ��������� ���������� ������� �� ���������� (?) 
double CTmtr::GetTA0(void)
{
   if(!DevIsOpen())
   {
      //m_adc_last_r=3.0; 
      m_adc_last=-1.0; 
   }
   if(m_check_enable)
   {
      m_adc_last_r=2.0;
   }
   return m_adc_last_r;
}

// �������� ��������� ���������� ������� ����� ���������� (?) 
double CTmtr::GetTA1(void)
{
   if(!DevIsOpen())
   {
     // m_adc_last=3.0;
      m_adc_last=-1.0; 
   }
   if(m_check_enable)
   {
      m_adc_last=2.0;
   }
   return m_adc_last;
}

// �������� ��������� ��������� ������� 
double CTmtr::GetVal(void)
{
   if(DevIsOpen())
   {
      return m_adc_last_val;
   }
   return 0.0;
}


// �������� �������� ��������� TA (TA/Cek) 
double CTmtr::GetVTA1(void)
{
   if(!DevIsOpen())
   {
/*
      m_adc_last=3.0;
      m_adc_last_p=3.0;
*/
      return 0;
   }
   if(m_dt==0)
   {
      return 0;
   }
   return (m_adc_last-m_adc_last_p)/(m_dt/1000000.0);
}

// �������� �������� ������� 
double CTmtr::GetVVal(void)
{
   if(!DevIsOpen())
   {
      return 0;
   }
   if(m_dt==0)
   {
      return 0;
   }
   return (m_adc_last_val-m_adc_last_val_p)/(m_dt/1000000.0);
}



// ����������� �������� ������� � ������ ������������ � ��������
double CTmtr::ta2val(double ta)
{
   if(m_tmtr_dev && m_tmtr_dev->IsOpen())
   {
      if(m_corr>=0) // mark5
      {
         return (m_tone-ta)*GetGain()*0.3; // winogr
      }
      // mark7
      double r0 = ta2r(m_tone);
      double r = ta2r(ta);
      double rmax = 13000; //1750*8.0;  //8*570*40/13;
      double val = (r0-r)/rmax;
      val *= GetGain();
      return val;
   }
   return 0.0;
}

// �������� �������� ������������� �� �������� ���������� (V)
double CTmtr::u2r(double u)
{
   if(m_tmtr_dev)
   {
      double r = m_tmtr_dev->u2r(u);
      r*=(1+m_trim/100.0); // ???
      return r;
   }
   return 0;
}

// �������� �������� TA �� �������� ������������� (Om)
double CTmtr::r2ta(double R)
{
/*
PC = 21250(TA - 0.941)/(6.5 - TA)
TA = PC/(PC + 21250)*5.559 + .941 
TA 	PC
1 	227
2 	5k
3 	12.5k
4 	26k
5 	57.5k
6 	215k
6.5 	infinity
*/
   double ta=R*1.0/(R*1.0 + 21250)*5.559 + .941;

   return ta;
}

// �������� �������� ������������� (Om) �� �������� TA
double CTmtr::ta2r(double ta)
{
   //TA = PC/(PC + 21250)*5.559 + .941 
   double r;

   r = 21250*(ta - 0.941)/(6.5 - ta);

   if(r>9999990.0)
   {
      r = 9999990.0;
   }
   if(r<0.001)
   {
      r = 0.001;
   }

   return r;
}

double CTmtr::ta2lr(double ta)
{
   double r;
   r = ta2r(ta);
   r = log10(r);
   if(r < 2.36)
   {
      r = 2.36;
   }
   return r;
}

double CTmtr::ta2gsr(double ta)
{
   double gsr = ta;

   //gsr = (ta - 0.94)*1.8;
   gsr = (ta - 1.0)*1.8182;

   if(gsr<0.0001)
   {
      gsr = 0.0001;
   }

   if(gsr>10.0)
   {
      gsr = 10.0;
   }

   return gsr;
}

// �������� �������� ��������� ������������� �� �������� TA

// �������� �������� TA �� �������� ����������
double CTmtr::u2ta(double u)
{
   double r;

   if(m_tmtr_dev)
   {
      r=m_tmtr_dev->u2r(u);
      r*=(1+m_trim/100.0); // ???
      return r2ta(r*1000.0);
   }
   return 0;
}


// ������ �������� ���������
int CTmtr::DevThreadStart()
{
   outlog(2,"TreadStart()") ;

   m_hMutex = CreateMutex( NULL, FALSE, NULL); // ������� ������� � ��������� ���������
   m_thread_loop=1;
   m_thread_end=0;
   _beginthread(CTmtr::TmtrProc, 0, (void *)this);
   return 1;
}


// ���������� �������� ���������
void CTmtr::DevThreadStop()
{
   int i;

   outlog(2,"DevThreadStop()") ;

   m_thread_loop=0;
   // ??? DevClose();
   i=20;
   while(m_thread_end==0)
   {
      outlog(2,"WaitDevThtreadStop()") ;
      Sleep(100);
      i--;
      if(i<=0)
      {
         break;
      }
   }
   CloseHandle( m_hMutex );

   outlog(2,"DevTreadStop() ok") ;
}

// ������ UDP �������
int CTmtr::SrvUdpThreadStart()
{
    // L"Mode\\UdpServer"
    if (papp_cfg->get_bool_value(L"Mode\\UdpServer") && GetDevType() != DEV_WIFI4)
    {
        outlog(2, "SrvUdpStart()");
        //  m_hMutex_srv = CreateMutex( NULL, FALSE, NULL); // ������� ������� � ��������� ���������
        m_thread_srv_udp_loop = 1;
        m_thread_srv_udp_end = 0;
        _beginthread(CTmtr::SrvUdpProc, 0, (void *)this);
        return 1;
    }
    else
    {
        m_thread_srv_udp_loop = 0;
        m_thread_srv_udp_end = 1;
        return 0;
    }
}

// ���������� UDP �������
void CTmtr::SrvUdpThreadStop()
{
   int i;

   outlog(2,"SrvUdpThreadStop()") ;

   m_thread_srv_udp_loop=0;
   i=10;
   while(m_thread_srv_udp_end==0)
   {
      outlog(2,"WaitSrvUdpThtreadStop()") ;
      Sleep(100);
      i--;
      if(i<=0)
      {
         break;
      }
   }

   outlog(2,"SrvUdpTreadStop() ok") ;
}


/*
 // ����������� ������� � ���������� 
void CTmtr::SrvSendEnable(int enable)
{
   m_send_enable=enable;
}
*/


int CTmtr::SrvWsThreadStart()
{
    // L"Mode\\WsServer"
    if (papp_cfg->get_bool_value(L"Mode\\WsServer"))
    {
        outlog(2, "SrvWsStart()");
        //  m_hMutex_srv = CreateMutex( NULL, FALSE, NULL); // ������� ������� � ��������� ���������
        m_thread_srv_ws_loop = 1;
        m_thread_srv_ws_end = 0;
        _beginthread(CTmtr::SrvWsProc, 0, (void *)this);
        return 1;
    }
    else
    {
        m_thread_srv_ws_loop = 0;
        m_thread_srv_ws_end = 1;
        return 0;
    }
}

// ���������� �������� ���������
void CTmtr::SrvWsThreadStop()
{
   int i;

   outlog(2,"SrvWsThreadStop()") ;

   m_thread_srv_ws_loop=0;
   i=10;
   while(m_thread_srv_ws_end==0)
   {
      outlog(2,"WaitSrvWsThtreadStop()") ;
      Sleep(100);
      i--;
      if(i<=0)
      {
         break;
      }
   }

   outlog(2,"SrvWsTreadStop() ok") ;
}


void CTmtr::AdcBufClear(void)
{
   adc_buf_io(2, 0, m_len_buf);
}


int CTmtr::get_adc_buf_ready(void)
{
   if(m_adc_next)
   {
      return m_len_buf;
   }
   else
   {
      return m_adc_in;
   }
}

int CTmtr::get_adc_buf(SBUF *buf, int len)
{
   return adc_buf_io(0, buf, len);
}

void CTmtr::RecStart(HANDLE fo)
{
   m_fo=fo;
}

void CTmtr::RecStop()
{
   m_fo=0;
}

void CTmtr::SetLastTA()
{
    m_fbtt_ind->clr();

    float fval = (float)ta2val(m_tone);

    m_adc_last_val = fval;
}


// private metods

void CTmtr::add_u(double val)
{
   double y;
   SBUF sb;

   m_adc_last_u=val/1000000.0;

   y=u2ta(val/1000000.0);
   //if(m_type_ta == 0)
   if(1)
   {
      if(y>6.4)
      {
         y=6.5;
      }
      if(y<1)
      {
         y=1.0;
      }
   }
   m_adc_last_r=y;

/*
   if(m_fbtt1)
   {
      val-=1500000.0;
      m_fbtt1->filtr(&y,&val);
      val=y;
      val+=1500000.0;
   }
*/

   if(m_fbtt2)
   {
      m_fbtt2->filtr(&y,&val);
   }
   else
   {
      y=val;
   }

   y=u2ta(y/1000000.0);
   if(y>6.4)
   {
      y=6.5;
   }
   
   if(y<1.0)
   {
      y=0.5;
   }

   m_adc_last_p=m_adc_last;
   m_adc_last=y;

   m_adc_last_val_p = m_adc_last_val;


   // ������ �������
   float fval;
   if(papp_cfg->get_bool_value(L"Mode\\needle_filter"))      
   {
      double v1,v2;
      v1 = ta2val(y);
      if(m_fbtt_ind)
      {
         m_fbtt_ind->filtr(&v2,&v1);
      }
      else
      {
         v2=v1;
      }
      m_adc_last_val = v2;
      fval = (float)v2;
   }
   else
   {
      fval = (float)ta2val(y);
      m_adc_last_val = fval;
   }

   sb.inst_ta=(float)y;
   sb.tone=(float)m_tone;

/*
double v=GetVVal();
v*=GetGain();
sb.val = v*0.1;
*/

   sb.gain=(float)(m_gain*m_booster);
   sb.val=fval;
   sb.ta_counter=(float)m_ta_counter;
   sb.nm_counter=(float)m_nm_counter;

   //sb.ta=m_tone;
   adc_buf_io(1, &sb, 1);
}

int CTmtr::adc_buf_io(int cmd, SBUF *buf, int len)
{
	int rc;
	int i;

	if (!m_adc)
		return 0;

	// ���� ������������ �������� ����� ��� ��� ���������� � ������.
	rc = WaitForSingleObject(m_hMutex, 5000L); // 5 ������ �� �������
	if (rc == WAIT_TIMEOUT) { 
		// �������. ������� �� ��� ����� �� �����������.
		outlog(1,"WaitForSingleObject(Mutex) timeout");
		return 0; 
	} else {
		// ������� �����������, � ��� ����� ��� �����. ����� ��������.
		switch(cmd) {
		case 0: 
			// rd 
			for (i = 0; i < len; i++)
				memcpy(&buf[i], &m_adc[i], sizeof(SBUF));
			break;

		case 1: 
			// wr
			for (i = 0; i < len; i++)
				adc_add(&buf[i]);
			break;

		case 2: 
			// clr
			for (i = 0; i < len; i++) {
				m_adc[i].inst_ta = 0;
				m_adc[i].tone = 0; 
				m_adc[i].gain = 0;
				m_adc[i].val = 0;
				m_adc[i].ta_counter = 0;
				m_adc[i].nm_counter = 0;
			}
			m_adc_next = 0;
			m_adc_in = 0;
			break;
		}
		
		ReleaseMutex(m_hMutex); // ����������� �������.
	}

	return m_adc_in;
}

// Copies only new data to the buffer provided
bool CTmtr::get_new_adc_buf(SBUF *buf, int buf_size, int *count, int *new_in)
{
	if (!m_adc || !buf || !count || !new_in || !buf_size)
		return false;

	// ���� ������������ �������� ����� ��� ��� ���������� � ������.
	int rc = WaitForSingleObject(m_hMutex, 5000L); // 5 ������ �� �������
	if (rc == WAIT_TIMEOUT) { 
		// �������. ������� �� ��� ����� �� �����������.
		outlog(1,"WaitForSingleObject(Mutex) timeout");
		return false; 
	} else {
		// ������� �����������, � ��� ����� ��� �����. ����� ��������.
		int len_exists = m_adc_in - *new_in;
		if (len_exists < 0)
			len_exists += m_len_buf;

		int len_copy = min(len_exists, buf_size);
		if (len_copy < buf_size)
			len_copy = len_copy;
		for (int i = 0; i < len_copy; i++) {
			int index = *new_in + i;
			if (index >= m_len_buf)
				index -= m_len_buf;

			memcpy(&buf[i], &m_adc[index], sizeof(SBUF));
		}

		*count = len_copy;
		*new_in = *new_in + len_copy;
		if (*new_in >= m_len_buf)
			*new_in -= m_len_buf;

		ReleaseMutex(m_hMutex); // ����������� �������.

		return len_exists > buf_size;
	}

	return false;
}

void CTmtr::adc_add(SBUF *sb)
{
	unsigned long Written;

	memcpy(&m_adc[m_adc_in], sb, sizeof(SBUF));

	m_adc_in++;
	if (m_adc_in >= m_len_buf) {
		m_adc_next = 1;
		m_adc_in = 0;
	}

	if (m_fo != INVALID_HANDLE_VALUE)
		WriteFile(m_fo, sb, sizeof(SBUF), &Written, 0);
}


// �������� �������� 
void CTmtr::filter_ini(void)
{
   int mode=0;
 
   outlog(1,"filter_ini() dt=%.1f f0=%.3f f01=%.3f",m_dt, m_f2, m_f1);

   if(m_f1>0)
   {
      m_fbtt1->ini(m_dt,m_f1,2,1,1);
   }
   else
   {
      m_fbtt1->ini(m_dt,m_f1,0,1,1);
   }

   if(m_f2>0 && m_filter_mode>0)
   {
      //m_fbtt2->ini(m_dt, m_f2*4, 2, 0, 1);
      m_fbtt2->ini(m_dt, m_f2*2, 2, 0, 1); // !!!
   }
   else
   {
      m_fbtt2->ini(m_dt, m_f2, 0, 0, 1);
   }

   if(papp_cfg->get_bool_value(L"Mode\\inertia_on"))
   {
      double a = papp_cfg->get_float_value(L"Mode\\overshoot"); 
      if(!papp_cfg->get_bool_value(L"Mode\\needle_rebound"))
      {
         a = papp_cfg->get_float_value(L"Mode\\overshoot0"); ;
      }
      m_fbtt_ind->ini(m_dt, m_f2*2, a);
   }
   else
   {
      m_fbtt_ind->ini(0.0, 0.0, 0.0);
      m_fbtt2->ini(m_dt, m_f2, 0, 0, 1); // !!!
   }
}


// ����� ��� ������ � �����������
void CTmtr::TmtrProc(void *p)
{
   CTmtr *pTmtr=(CTmtr*)p;
   double u;
   int n;
   int i;

   outlog(1,"TmtrProc() - start");
   if(!p)
   {
      outlog(1,"TmtrProc() - end");
      pTmtr->m_thread_end=1;
      return;
   }

   if(!pTmtr->m_tmtr_dev)
   {
      outlog(1,"Tmeter device not found");
      outlog(1,"TmtrProc() - end");
      Sleep(100);
      pTmtr->m_thread_end=1;
      return;
   }

  timeBeginPeriod(1); // ���������� ������� - 1 ��

   if(pTmtr->m_priority_enable)
   {
      if (!SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST))
      {
         outlog(1,"Tmtr Error SetThreadPriority().");
      }
      else
      {
         outlog(1,"SetThreadPriority() for Tmtr OK");
      }
   }

   while(pTmtr->m_thread_loop)
   {
      if(!pTmtr->DevIsOpen())
      {
         pTmtr->DevOpen();

         if(pTmtr->DevIsOpen())
         {
            pTmtr->m_call_back();
         }

      }

      if(!pTmtr->DevIsOpen())
      {
         for(i=0;i<20;i++)
         {
            if(!pTmtr->m_thread_loop)
            {
               break;
            }
            Sleep(100);
            pTmtr->m_call_back();
         }
      }
      
      if(pTmtr->DevIsOpen())
      {
         pTmtr->DevSetDt(pTmtr->m_n_dt);
      }
      
      n=0;
      while(pTmtr->m_thread_loop && pTmtr->DevIsOpen())
      {
         if(pTmtr->m_tmtr_dev->ReadU(&u))
         {
            //if(pwndMain->m_view.m_started)
            if(pTmtr->m_enabeld)
            {
               outlog(2,"%.3f [mV]", u/1000.0);

               srv_send_r(pTmtr->m_tmtr_dev->u2r(u/1000000.0));  // winogr
               
               pTmtr->add_u(u);

               n++;
               if(n>=pTmtr->m_n_call)
               {
                  if(pTmtr->m_call_back && pTmtr->m_n_call)
                  {
                     pTmtr->m_call_back();
                  }
                  n=0;
               }
                  

            }
         }
         else
         {
            Sleep(0);
         }
      }
      pTmtr->m_call_back();
   }

   if(pTmtr->DevIsOpen())
   {
      outlog(1,"End while()");
      pTmtr->DevClose();
   }

   timeEndPeriod(1); // ���������� ������� - 1 ��

   pTmtr->m_thread_end=1;
   outlog(1,"TmtrProc() - end") ;
}


void CTmtr::SrvUdpProc(void *p)
{
   unsigned short port = 5555;
   CTmtr *pTmtr=(CTmtr*)p;

   outlog(1,"SrvUdpProc() - start");

   srv_open(port);

   if(!p)
   {
      outlog(1,"SrvUdpProc() - end");
      pTmtr->m_thread_srv_udp_end=1;
      return;
   }

   while(pTmtr->m_thread_srv_udp_loop)
   {
      srv_task();
   }

   srv_close();

   pTmtr->m_thread_srv_udp_end=1;
   outlog(1,"SrvUdpProc() - end") ;
}


void CTmtr::SrvWsProc(void *p)
{
   unsigned short port = 8080;

   CTmtr *pTmtr = (CTmtr*)p;
   int ll = papp_cfg->get_int_value(L"Options\\ll");

   outlog(1,"SrvWsProc() - start");

   if(!ws_open(port, ll))
   {
      outlog(1,"SrvWsProc() - end");
      pTmtr->m_thread_srv_ws_end = 1;
      return; 
   }

   while(pTmtr->m_thread_srv_ws_loop)
   {
      //srv_task();
      ws_task();
   }

   ws_cancel();

   ws_close();

   pTmtr->m_thread_srv_ws_end=1;
   outlog(1,"SrvWsProc() - end") ;
}

// ������� ����������� BLE  
  // Bluetooth LE device interface GUID  {781aee18-7733-4ce4-adb0-91f41c67b592}
  DEFINE_GUID(GUID_BLUETOOTHLE_DEVICE_INTERFACE, 0x781aee18, 0x7733, 0x4ce4, 0xad, 0xd0, 0x91, 0xf4, 0x1c, 0x67, 0xb5, 0x92);
int CTmtr::TestBLE(void)
{
    GUID BluetoothInterfaceGUID = GUID_BLUETOOTHLE_DEVICE_INTERFACE;
    HDEVINFO hDI;
    SP_DEVICE_INTERFACE_DATA did;
    //SP_DEVINFO_DATA dd;
    int rc = 0;

    hDI = SetupDiGetClassDevs(&BluetoothInterfaceGUID, NULL, NULL, DIGCF_DEVICEINTERFACE | DIGCF_PRESENT);
    if (hDI == INVALID_HANDLE_VALUE)
    {
        return NULL;
    }

    did.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
    //dd.cbSize = sizeof(SP_DEVINFO_DATA);
    for (DWORD i = 0; SetupDiEnumDeviceInterfaces(hDI, NULL, &BluetoothInterfaceGUID, i, &did); i++)
    {
        SP_DEVINFO_DATA dd;
        dd.cbSize = sizeof(SP_DEVINFO_DATA);

        SP_DEVICE_INTERFACE_DETAIL_DATA DeviceInterfaceDetailData;
        DeviceInterfaceDetailData.cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

        DWORD size = 0;
        if (!SetupDiGetDeviceInterfaceDetail(hDI, &did, NULL, 0, &size, 0))
        {
            int err = GetLastError();
            if (err == ERROR_NO_MORE_ITEMS) 
            {
               break;
            }
            PSP_DEVICE_INTERFACE_DETAIL_DATA pInterfaceDetailData = (PSP_DEVICE_INTERFACE_DETAIL_DATA)GlobalAlloc(GPTR, size);
            pInterfaceDetailData->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);
            if (SetupDiGetDeviceInterfaceDetail(hDI, &did, pInterfaceDetailData, size, &size, &dd))
            {
               //wprintf(_T("%s\n"), pInterfaceDetailData->DevicePath);
               outlog(1,"BLE found");
               rc = 1;
            }
            GlobalFree(pInterfaceDetailData);
            break;
        }
    }

    SetupDiDestroyDeviceInfoList(hDI);

    if(!rc)
    {
       outlog(1,"BLE not found");
    }

    return rc;
}
