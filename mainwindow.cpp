#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "ctmtr_hid.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    dev = new CTmtr_hid();
}

MainWindow::~MainWindow()
{
    delete ui;
    if (dev)
        delete dev;
}

void MainWindow::on_openButton_clicked()
{
    dev->open();
}

void MainWindow::on_closeButton_clicked()
{
    dev->close();
}

void MainWindow::on_readButton_clicked()
{
    double u;
    if (dev->readU(&u)) {
        ui->uEdit->setText(QString("%1").arg(u));
    }
}
