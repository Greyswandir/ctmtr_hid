#include "ctmtr_hid.h"
#include <QDebug>
#include "hidapi.h"

CTmtr_hid::CTmtr_hid()
{
    m_R0 = 83.0;     // kOm
    m_E0 = 0.0;      // V
    m_E1 = 3.205;    // V
    m_E = m_E1 - m_E0; // V

    m_f_open = 0;
    m_device = NULL;
    m_dt = 0;
    m_dev_code = -1;
    m_deviceVersionNumber = -1;
    m_deviceBaseName = QString("Theta-Meter");
}

CTmtr_hid::~CTmtr_hid()
{
    close();
}

QString CTmtr_hid::getDevName (void) // получить название устройства
{
    QString version = QString(" 2.00");
    if(m_deviceVersionNumber != -1 && m_deviceVersionNumber >= 0x0200)
    {
        int hi = (m_deviceVersionNumber&0xFF00) >> 8;
        int lo = m_deviceVersionNumber&0xFF;
        version = QString(" %1.%2").arg(hi).arg(lo,2,10,QChar('0'));
    }
    return m_deviceBaseName + version;
}

int CTmtr_hid::getDevVersion (void) // получить версию устройства (-1 Unknown)
{
    return m_deviceVersionNumber;
}

int CTmtr_hid::getDevCode (void) // получить код устройства (-1 Unknown)
{
    return m_dev_code;
}

int CTmtr_hid::getDevPower (void) // получить уровень заряда батареи
{
    return -1;
}

int CTmtr_hid::getDevState (void) // получить состояние устойства 0-ok
{
    return 0;
}

int CTmtr_hid::getDTn (int n) // получить интервал дискретизации по номеру (мк—)
{
    int baseDt = 1000000;
    int dt=baseDt/62;
    int dtDivFreq [] = {60, 50, 120};
    if ((m_deviceVersionNumber == 0x0300) && ((n >=0) && (n<=2))) {
        dt = baseDt / dtDivFreq[n];
    }
    return dt;
}

int CTmtr_hid::open (void) // открыть устройтсво
{
    int result = 0;
    unsigned int vid1 = 0x0483, pid1 = 0x5750;
    unsigned int vid2 = 0x1fc9, pid2 = 0x0003;
    hid_device_info info;
    int res = hid_init();
    m_device = hid_open(vid1, pid1, NULL);
    if (m_device == NULL) {
        qDebug() << QString("Device not found for vid=0x") + QString("%1").arg(vid1,0,16) +
                    QString(" pid=0x") + QString("%1").arg(pid1,0,16);
        m_device = hid_open(vid2, pid2, NULL);
        if (m_device == NULL) {
            qDebug() << QString("Device not found for vid=0x") + QString("%1").arg(vid2,0,16) +
                        QString(" pid=0x") + QString("%1").arg(pid2,0,16);
        }
        else {
            info = hid_enumerate(vid2,pid2);
        }
    }
    else {
        info = hid_enumerate(vid1,pid1);
    }
    if (m_device != NULL) {
        int len;
        wchar_t buf[200];
        len = hid_get_serial_number_string(m_device, buf, 200);
        buf[len] = 0;
        qDebug() << buf;
        m_f_open = 1;
        result = 1;
        m_deviceVersionNumber = info.release_number;
        qDebug() << "device opened revision=" << m_deviceVersionNumber;
    }
    return result;
}

void CTmtr_hid::close (void) // закрыть устройтсво
{
    if(m_device) {
        hid_close(m_device);
        m_device = 0;
    }
    hid_exit();
    m_f_open = 0;
}

int CTmtr_hid::isOpen (void) // рроверка IsOpen
{
    return m_f_open;
}

int CTmtr_hid::setDT (int n) // установка частоты цифровки
{
    unsigned char bcmd [3] = {0, 0x46, 0};
    unsigned char dtCmds [] = {0x31, 0x32, 0x33};
    int rc = 0;

    if(m_deviceVersionNumber == 0x0300)
    {
        if ((n >= 0) && (n <= 2))
        {
            bcmd [2] = dtCmds[n];
            rc = sendCmd ((char *) bcmd, sizeof (bcmd));
            //outlog (1,"SendCmd() rc=%d",rc);
            if (rc) m_dt=getDTn(n);
        }
    }

    return rc;
}

int CTmtr_hid::getDT (void) // получить интервал дискретизации (мк—)
{
    return m_dt;
}

int CTmtr_hid::readU (double *u) // получить напряжение на датчике (мк+)
{
    int result = 0;
    const double df = 3.3*1000000.0/0x1000000;
    unsigned char brcv[64];
    int rlen;
    int len;
    rlen = 17;
    brcv[0] = 0;
    if (m_device) {
        len = hid_read(m_device, brcv, rlen);
        qDebug() << "Readed length=" << len;
        qDebug() << "Readed data" << brcv;
        if(len>0) {
            if(len < 5)
            {
                qDebug() << "Failed: readed len=" << len;
            }
            else if(brcv[1]!=0x01)
            {
                qDebug() << "Failed: brcv[1]=" << brcv[1];
            }
            else
            {
                *u=0;
                *u+=brcv[3]*0x10000;
                *u+=brcv[4]*0x100;
                *u+=brcv[5];

                *u*=df;
                qDebug() << "u = " << QString("%1").arg(*u,0,'g',3);
                m_dev_code = 0x0200 + brcv [2];
                result = 1;
            }
        }
    }
    return result;
}

double CTmtr_hid::u2r (double u) // получить сопротивление(kOm) по значению u(B)
{
    double result = 99999.999;
    if(m_E > u)
    {
        result = (u * m_R0)/(m_E - u);
    }
    return result;
}

int CTmtr_hid::sendCmd (char *bsnd, int slen)
{
    int len;
    if (m_device) {
        len = hid_write(m_device, (unsigned char*)bsnd,slen);
        if(len!=slen)
        {
            qDebug() << "Failed: bulk transfer for write failed";
        }
        qDebug() << "write length" << len;
    }
    return 1;
}

